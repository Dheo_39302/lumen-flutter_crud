import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter_crud/type_service.dart';
import 'package:flutter_crud/utils/response.dart';

Future<BsSelectBoxResponse> selectType(Map<String, String> params) async {
  TypeService typeService = TypeService();
  Response response = await typeService.select(params);
  if(response.result!)
    return BsSelectBoxResponse.createFromJson(response.data);

  return BsSelectBoxResponse(options: []);
}