import 'package:flutter_crud/utils/config.dart';
import 'package:flutter_crud/utils/repository_crud.dart';

class TypeService extends RepositoryCRUD {

  @override
  // TODO: implement api
  String get api => '${Config.api}/types';

}