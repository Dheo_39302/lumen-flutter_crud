import 'package:fluro/fluro.dart';
import 'package:flutter_crud/routes/type_route.dart';

abstract class RoutePage {
  void routes();
}

class Routes extends FluroRouter {

  Routes() {
    TypeRoute(this).routes();
  }
}