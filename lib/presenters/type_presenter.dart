import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crud/constants/constants.dart';
import 'package:flutter_crud/helpers/functions.dart';
import 'package:flutter_crud/models/type_model.dart';
import 'package:flutter_crud/type_service.dart';
import 'package:flutter_crud/utils/view_contract.dart';
import 'package:flutter_crud/views/type/source/_datasource.dart';
import 'package:flutter_crud/views/type/widget/type_form.dart';

class TypePresenter extends TypeFormSource {

  GlobalKey<State> _formState = GlobalKey<State>();

  TypePresenter(this.viewContract);

  final ViewContract viewContract;

  TypeService typeService = TypeService();
  TypeModel typeModel = TypeModel();

  TypeDataSource typeSource = TypeDataSource();
  BsDatatableController typeController = BsDatatableController();

  void setLoading(bool loading) {
    isLoading = loading;
    viewContract.updateState();

    if(_formState.currentState != null)
      _formState.currentState!.setState(() {});
  }

  Map<String, String> getDatas() {
    return {
      'parentid': selectParent.getSelectedAsString() != '' ? selectParent.getSelectedAsString() : '0',
      'typecd': inputCode.text,
      'typenm': inputName.text,
      'typeseq': inputSequence.text,
      'description': inputDescription.text,
    };
  }

  void setData(TypeModel type) {
    typeModel = type;

    if(typeModel.parentid != 0)
      selectParent.setSelected(BsSelectBoxOption(value: typeModel.parentid, text: Text(parseString(typeModel.parent.typenm))));

    inputCode.text = parseString(typeModel.typecd);
    inputName.text = parseString(typeModel.typenm);
    inputSequence.text = parseString(typeModel.typeseq);
    inputDescription.text = parseString(typeModel.description);

    setLoading(false);
  }

  void resetData() {
    selectParent.clear();
    inputCode.clear();
    inputName.clear();
    inputSequence.clear();
    inputDescription.clear();
  }

  Future datatables(BuildContext context, Map<String, String> params) {
    return Future(() {
      typeService.datatables(params).then((value) {
        if(value.result!) {
          typeSource = TypeDataSource(
            response: BsDatatableResponse.createFromJson(value.data),
            onEdit: (id) => edit(context, id),
            onDelete: (id) => delete(context, id),
          );
          setLoading(false);
        }
      });
    });
  }

  void add(BuildContext context) {
    resetData();
    setLoading(false);

    showDialog(
      context: context,
      builder: (context) => TypeFormModal(
        key: _formState,
        presenter: this,
        onSubmit: () => store(context),
      )
    );
  }

  void store(BuildContext context) {
    setLoading(true);
    typeService.store(getDatas()).then((res) {
      setLoading(false);

      if(res.result!) {
        Navigator.pop(context);
        typeController.refresh();
      }
    });
  }

  void edit(BuildContext context, int id) {
    resetData();
    setLoading(true);

    showDialog(
      context: context,
      builder: (context) => TypeFormModal(
        key: _formState,
        presenter: this,
        onSubmit: () => update(context, id),
      ),
    );

    typeService.show(id).then((res) {
      if(res.result!) {
        setData(TypeModel.fromJson(res.data));
      }
    });
  }

  void update(BuildContext context, int id) {
    setLoading(true);
    typeService.update(id, getDatas()).then((res) {
      setLoading(false);

      if(res.result!) {
        Navigator.pop(context);
        typeController.refresh();
      }
    });
  }

  void delete(BuildContext context, int id) {
    showDialog(
      context: context,
      builder: (context) => BsModal(
        context: context,
        dialog: BsModalDialog(
          size: BsModalSize.sm,
          mainAxisAlignment: MainAxisAlignment.center,
          child: BsModalContent(
            children: [
              BsModalContainer(title: Text('Konfirmasi Hapus'), closeButton: true),
              BsModalContainer(child: Column(
                children: [
                  Text('Apakahan anda yakin?'),
                ],
              )),
              BsModalContainer(
                mainAxisAlignment: MainAxisAlignment.end,
                actions: [
                  BsButton(
                    disabled: isLoading,
                    margin: EdgeInsets.only(right: 5.0),
                    label: Text(DBText.buttonModalCancel),
                    prefixIcon: DBIcon.buttonModalCancel,
                    style: BsButtonStyle.danger,
                    size: BsButtonSize.btnSm,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  BsButton(
                    disabled: isLoading,
                    label: Text(isLoading ? DBText.buttonProcessing : DBText.buttonModalDelete),
                    prefixIcon: isLoading ? DBIcon.buttonProcessing : DBIcon.buttonDelete,
                    style: BsButtonStyle.primary,
                    size: BsButtonSize.btnSm,
                    onPressed: () {
                      typeService.delete(id).then((res) {
                        setLoading(false);
                        Navigator.pop(context);
                        typeController.refresh();
                      });
                    },
                  ),
                ]
              )
            ],
          ),
        ),
      ),
    );
  }

}