import 'dart:convert';

import 'package:flutter_crud/constants/constants.dart';
import 'package:flutter_crud/utils/config.dart';
import 'package:flutter_crud/utils/response.dart';
import 'package:http/http.dart' as http;

class Repository {
  static http.Client _client = http.Client();

  static Future<Response> post(url, {
    Map<String, String>? headers, Map<String, String>? body, Encoding? encoding
  }) async {

    try {
      Map<String, String> merger = new Map<String, String>();

      merger.addAll({
        'Content-Type': 'application/x-www-form-urlencoded',
      });

      if(headers != null) merger.addAll(headers);

      final response = await _client.post(Uri.parse(url), headers: merger, body: body, encoding: encoding);

      return Response.fromJSON(json.decode(response.body));
    } catch(e) {
      return Response(result: false, status: 500, message: DBMessage.requestFailed);
    }
  }

  static Future<Response> put(url, {
    Map<String, String>? headers, body, Encoding? encoding
  }) async {

    try {
      Map<String, String> merger = new Map<String, String>();

      merger.addAll({
        'Content-Type': 'application/x-www-form-urlencoded',
      });

      if(headers != null) merger.addAll(headers);

      final response = await _client.put(url, headers: merger, body: body, encoding: encoding);

      return Response.fromJSON(json.decode(response.body));
    } catch(e) {
      return Response(result: false, status: 500, message: DBMessage.requestFailed);
    }
  }

  static Future<Response> get(url, {
    Map<String, String>? headers, Map<String, String>? body, Encoding? encoding
  }) async {

    try {

      Uri newUrl;
      if(url.toString().indexOf('https') > -1) {
        String hostDir = Config.api.toString().replaceAll('https://', '');
        String host = hostDir.indexOf('/') > -1 ? hostDir.substring(0, hostDir.indexOf('/')) : hostDir;
        String dir = hostDir.indexOf('/') > - 1 ? hostDir.replaceAll(host, '').substring(1) : '';
        String request = dir + url.toString().replaceAll(Config.api, '');

        newUrl = Uri.https(host, request, body);
      } else {
        String hostDir = Config.api.toString().replaceAll('http://', '');
        String host = hostDir.indexOf('/') > -1 ? hostDir.substring(0, hostDir.indexOf('/')) : hostDir;
        String dir = hostDir.indexOf('/') > - 1 ? hostDir.replaceAll(host, '').substring(1) : '';
        String request = dir + url.toString().replaceAll(Config.api, '');

        newUrl = Uri.http(host, request, body);
      }

      Map<String, String> merger = new Map<String, String>();

      merger.addAll({
        'Content-Type': 'application/x-www-form-urlencoded',
      });

      if(headers != null) merger.addAll(headers);

      final response = await _client.get(newUrl, headers: merger);

      return Response.fromJSON(json.decode(response.body));
    } catch(e) {
      return Response(result: false, status: 500, message: DBMessage.requestFailed);
    }
  }

  static Future<Response> delete(url, {
    Map<String, String>? headers, Map<String, String>? body, Encoding? encoding
  }) async {

    try {
      Uri newUrl;
      if(url.toString().indexOf('https') > -1) {
        String hostDir = Config.api.toString().replaceAll('https://', '');
        String host = hostDir.indexOf('/') > -1 ? hostDir.substring(0, hostDir.indexOf('/')) : hostDir;
        String dir = hostDir.indexOf('/') > - 1 ? hostDir.replaceAll(host, '').substring(1) : '';
        String request = dir + url.toString().replaceAll(Config.api, '');

        newUrl = Uri.https(host, request, body);
      } else {
        String hostDir = Config.api.toString().replaceAll('http://', '');
        String host = hostDir.indexOf('/') > -1 ? hostDir.substring(0, hostDir.indexOf('/')) : hostDir;
        String dir = hostDir.indexOf('/') > - 1 ? hostDir.replaceAll(host, '').substring(1) : '';
        String request = dir + url.toString().replaceAll(Config.api, '');

        newUrl = Uri.http(host, request, body);
      }

      Map<String, String> merger = new Map<String, String>();

      merger.addAll({
        'Content-Type': 'application/json; charset=UTF-8',
      });

      if(headers != null) merger.addAll(headers);

      final response = await _client.delete(newUrl, headers: merger);

      return Response.fromJSON(json.decode(response.body));
    } catch(e) {
      return Response(result: false, status: 500, message: DBMessage.requestFailed);
    }
  }

  static Future<Response> multiPart(url, {Map<String, String>? fields, List<http.MultipartFile>? files}) async {
    try {

      final req = http.MultipartRequest('POST', Uri.parse(url));

      if(fields != null)
        req.fields.addAll(fields);

      if(files != null)
        req.files.addAll(files);

      http.Response response = await http.Response.fromStream(await req.send());

      return Response.fromJSON(json.decode(response.body));
    } catch(e) {
      return Response(result: false, status: 500, message: DBMessage.requestFailed);
    }
  }
}

abstract class RepositoryCRUD {
  String get api;

  Future<Response> select(Map<String, String> params) {
    return Repository.get("$api/select", body: params);
  }

  Future<Response> selectApi(String api, Map<String, String> params) {
    return Repository.get("${Config.api}/select/$api", body: params);
  }

  Future<Response> datatables(Map<String, String> params) {
    return Repository.post('$api/datatables', body: params);
  }

  Future<Response> index(Map<String, String> params) {
    return Repository.get(api, body: params);
  }

  Future<Response> store(Map<String, String>? datas, {List<http.MultipartFile>? files}) {
    return Repository.multiPart('$api', fields: datas, files: files);
  }

  Future<Response> show(int id, {Map<String, String>? params}) {
    return Repository.get("$api/$id", body: params);
  }

  Future<Response> update(int id, Map<String, String>? datas, {List<http.MultipartFile>? files}) {
    return Repository.multiPart("$api/update/$id", fields: datas, files: files);
  }

  Future<Response> delete(int id, {Map<String, String>? params}) {
    return Repository.post("$api/delete/$id", body: params);
  }
}