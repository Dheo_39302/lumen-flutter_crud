class Response {
  bool? result;
  int? status;
  String? message;
  int? code;
  dynamic data;

  Response({
    this.result,
    this.status,
    this.code,
    this.message,
    this.data,
  });

  factory Response.fromJSON(Map<String, dynamic> map) {
    return Response(
      result: map["result"],
      status: map["status"],
      code: map["code"],
      message: map["message"],
      data: map["data"],
    );
  }
}