import 'package:fluro/fluro.dart';
import 'package:flutter_crud/routes.dart';
import 'package:flutter_crud/views/type/widget/type.dart';

class TypeRoute implements RoutePage {

  static String routeKey = 'crud';
  static String index = '/';

  TypeRoute(FluroRouter router) : _router = router;

  late FluroRouter _router;

  @override
  void routes() {
    _router.define(index, handler: Handler(
      handlerFunc: (context, parameters) {
        return TypeView();
      },
    ));
  }

}