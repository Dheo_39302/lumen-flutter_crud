part of datasource;

class TypeText {

  static String title = 'Tipe';

  static String formParent = 'Induk';
  static String formCode = 'Kode';
  static String formSequence = 'Urutan';

  static String tableCode = 'typecd';
  static String tableName = 'typenm';
}