library datasource;

import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_crud/constants/constants.dart';
import 'package:flutter_crud/helpers/functions.dart';
import 'package:flutter_crud/helpers/select_api.dart';
import 'package:flutter_crud/models/type_model.dart';
import 'package:flutter_crud/presenters/type_presenter.dart';

part 'typetext.dart';
part 'formsource.dart';
part 'typesource.dart';