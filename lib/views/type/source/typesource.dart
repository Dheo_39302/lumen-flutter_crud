part of datasource;

class TypeDataSource extends BsDatatableSource {

  BsDatatableResponse response;
  ValueChanged<int>? onEdit;
  ValueChanged<int>? onDelete;

  TypeDataSource({
    this.response = const BsDatatableResponse(),
    this.onEdit,
    this.onDelete,
  });
  
  static List<BsDataColumn> columns = <BsDataColumn>[
    BsDataColumn(label: Text(DBText.tableCellNo), width: 100.0, searchable: false, orderable: false),
    BsDataColumn(label: Text(TypeText.formCode), columnData: TypeText.tableCode, columnName: TypeText.tableCode),
    BsDataColumn(label: Text(DBText.formName), columnData: TypeText.tableName, columnName: TypeText.tableName),
    BsDataColumn(label: Text(DBText.tableCellAction), width: 200.0, searchable: false, orderable: false),
  ];

  List<TypeModel> get types => List<TypeModel>
      .from(response.data.map((e) => TypeModel.fromJson(e)).toList());

  @override
  // TODO: implement countData
  int get countData => response.countData;

  @override
  // TODO: implement countDataPage
  int get countDataPage => types.length;

  @override
  // TODO: implement countFiltered
  int get countFiltered => response.countFiltered;

  @override
  BsDataRow getRow(int index) {
    TypeModel type = types[index];
    return BsDataRow(
      index: index,
      cells: <BsDataCell>[
        BsDataCell(Text('${response.start + index + 1}')),
        BsDataCell(Text('${notNull(type.typecd)}')),
        BsDataCell(Text('${notNull(type.typenm)}')),
        BsDataCell(Row(children: [
          BsButton(
            margin: EdgeInsets.only(right: 5.0),
            prefixIcon: DBIcon.buttonEdit,
            size: BsButtonSize.btnIconSm,
            style: BsButtonStyle.primary,
            onPressed: () {
              if(onEdit != null)
                onEdit!(type.id);
            },
          ),
          BsButton(
            prefixIcon: DBIcon.buttonDelete,
            size: BsButtonSize.btnIconSm,
            style: BsButtonStyle.danger,
            onPressed: () {
              if(onDelete != null)
                onDelete!(type.id);
            },
          ),
        ]))
      ]
    );
  }

}