import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_crud/constants/constants.dart';
import 'package:flutter_crud/presenters/type_presenter.dart';
import 'package:flutter_crud/views/type/source/_datasource.dart';

class TypeFormModal extends StatefulWidget {

  const TypeFormModal({
    Key? key,
    required this.presenter,
    this.onClose,
    this.onSubmit
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _TypeFormModalState();
  }

  final TypePresenter presenter;

  final VoidCallback? onClose;

  final VoidCallback? onSubmit;

}

class _TypeFormModalState extends State<TypeFormModal> {

  late TypeForm _typeForm;

  @override
  void initState() {
    _typeForm = TypeForm(
      context: context,
      presenter: widget.presenter,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BsModal(
      context: context,
      dialog: BsModalDialog(
        child: BsModalContent(
          children: [
            BsModalContainer(title: Text(DBText.formTitle(TypeText.title)), closeButton: true),
            BsModalContainer(
              child: Column(
                children: [
                  _typeForm.selectParent(),
                  _typeForm.inputCode(),
                  _typeForm.inputName(),
                  _typeForm.inputSequence(),
                  _typeForm.inputDescription()
                ],
              ),
            ),
            BsModalContainer(
              mainAxisAlignment: MainAxisAlignment.end,
              actions: [
                BsButton(
                  margin: EdgeInsets.only(right: 5.0),
                  label: Text(DBText.buttonModalCancel),
                  prefixIcon: DBIcon.buttonModalCancel,
                  style: BsButtonStyle.danger,
                  size: BsButtonSize.btnMd,
                  onPressed: () {
                    Navigator.pop(context);
                    if(widget.onClose != null)
                      widget.onClose!();
                  },
                ),
                BsButton(
                  label: Text(DBText.buttonModalSave),
                  prefixIcon: DBIcon.buttonModalSave,
                  style: BsButtonStyle.primary,
                  size: BsButtonSize.btnMd,
                  onPressed: () {
                    if(widget.onSubmit != null)
                      widget.onSubmit!();
                  },
                ),
              ]
            )
          ],
        ),
      )
    );
  }

}