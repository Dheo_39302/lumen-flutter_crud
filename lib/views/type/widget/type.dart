import 'package:bs_flutter/bs_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crud/constants/constants.dart';
import 'package:flutter_crud/presenters/type_presenter.dart';
import 'package:flutter_crud/utils/view_contract.dart';
import 'package:flutter_crud/views/type/source/_datasource.dart';

class TypeView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TypeViewState();
  }

}

class _TypeViewState extends State<TypeView> implements ViewContract {

  late TypePresenter _presenter;

  @override
  void initState() {
    _presenter = TypePresenter(this);
    super.initState();
  }

  @override
  void updateState() {
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Flutter CRUD')),
      body: Scrollbar(child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0, bottom: 20.0),
              child: BsCard(
                children: [
                  BsCardContainer(title: Text('Master Tipe'), actions: [
                    BsButton(
                      label: Text(DBText.buttonAdd),
                      prefixIcon: DBIcon.buttonAdd,
                      style: BsButtonStyle.primary,
                      onPressed: () => _presenter.add(context),
                    )
                  ]),
                  BsCardContainer(child: Column(
                    children: [
                      BsDatatable(
                        title: Text('Data Tipe'),
                        source: _presenter.typeSource,
                        controller: _presenter.typeController,
                        serverSide: (params) => _presenter.datatables(context, params),
                        columns: TypeDataSource.columns,
                      )
                    ],
                  ))
                ],
              ),
            )
          ],
        ),
      )),
    );
  }

}